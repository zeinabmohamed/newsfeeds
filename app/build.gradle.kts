import Versions.Versions

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}
android {
    compileSdkVersion(Versions.Android.compileSdkVersion)
    buildToolsVersion(Versions.Android.buildToolsVersion)
    defaultConfig {
        applicationId = Versions.Android.applicationId
        minSdkVersion(Versions.Android.minSdkVersion)
        targetSdkVersion(Versions.Android.targetSdkVersion)
        versionCode = Versions.Android.appVersionCode
        versionName = Versions.Android.appVersionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField( "String", "BASE_URL", Config.BASE_UR)

    }


    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    dataBinding {
        isEnabled = true
    }


    compileOptions {
        sourceCompatibility  = JavaVersion.VERSION_1_8
        targetCompatibility =  JavaVersion.VERSION_1_8
    }



}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}")
    implementation("androidx.appcompat:appcompat:${Versions.appcompat}")
    implementation("androidx.core:core-ktx:${Versions.appcompat}")
    implementation("androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}")
    testImplementation("junit:junit:${Versions.Test.junit_version}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${Versions.Test.espresso_core_version}")
    androidTestImplementation("androidx.test.ext:junit:${Versions.Test.testExtJunit}")
    androidTestImplementation("androidx.test.ext:junit-ktx:${Versions.Test.testExtJunit}")

    implementation("androidx.navigation:navigation-fragment-ktx:${Versions.nav_version}")
    implementation("androidx.navigation:navigation-ui-ktx:${Versions.nav_version}")

    implementation("com.google.android.material:material:${Versions.material_desgin_version}")

    implementation("androidx.lifecycle:lifecycle-extensions:${Versions.life_cycle_owner_version}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.life_cycle_owner_version}")

    implementation ("com.google.code.gson:gson:2.8.5")

    implementation ("com.squareup.retrofit2:retrofit:${Versions.retrofit_version}")
    implementation ("com.squareup.retrofit2:converter-gson:${Versions.retrofit_version}")

    implementation ("com.github.bumptech.glide:glide:${Versions.glide_version}")
    kapt ("com.github.bumptech.glide:compiler:${Versions.glide_version}")




}
