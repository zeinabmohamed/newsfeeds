package com.zm.org.newsfeeds.base.view

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt


class SpacesItemDecoration(private val context : Context , @DimenRes private val spaceRes: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {

        val spaceInPixels: Int = context.resources.getDimension(spaceRes).roundToInt()


        outRect.left = spaceInPixels
        outRect.right = spaceInPixels
        outRect.bottom = spaceInPixels
        outRect.top = spaceInPixels



    }
}
