package com.zm.org.newsfeeds.model

import com.zm.org.newsfeeds.model.cloud.NetworkManager
import com.zm.org.newsfeeds.model.dto.News
import com.zm.org.newsfeeds.model.dto.NewsFeedsResponse

/**
 * Stories data model handle data retrieving from cloud or local storage
 */
class NewsFeedsDataModel {


    /**
     * Load stories list from cloud
     */

    suspend fun loadStories()  = NetworkManager.newsFeedsServices.loadStories()

    suspend fun loadNews() = NetworkManager.newsFeedsServices.loadNews()
    suspend fun loadOffers() = NetworkManager.newsFeedsServices.loadOffers()
}