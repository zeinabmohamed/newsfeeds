package com.zm.org.newsfeeds.model.cloud

import com.zm.org.newsfeeds.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object NetworkManager {


    private val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()


    val newsFeedsServices by lazy { retrofit.create(NewsFeedsServices::class.java) }

}