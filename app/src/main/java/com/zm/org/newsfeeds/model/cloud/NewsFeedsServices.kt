package com.zm.org.newsfeeds.model.cloud

import com.zm.org.newsfeeds.model.dto.News
import com.zm.org.newsfeeds.model.dto.NewsFeedsResponse
import com.zm.org.newsfeeds.model.dto.Offer
import com.zm.org.newsfeeds.model.dto.Story
import retrofit2.http.GET

interface NewsFeedsServices {

    @GET("stories")
    suspend fun loadStories(): NewsFeedsResponse<List<Story>>

    @GET("news")
    suspend fun loadNews(): NewsFeedsResponse<List<News>>

    @GET("offers")
    suspend fun loadOffers(): NewsFeedsResponse<List<Offer>>
}