package com.zm.org.newsfeeds.util

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


/**
 * load Url image to [ImageView] using Glide 3'rd party .
 * @param url full formatted  HTTP image url .
 * @author Zeinab Abdelmawla
 */
@BindingAdapter(value = ["loadImageUrl", "circular"], requireAll = false)
fun ImageView.loadImage(url: String?, isCircular: Boolean = false) {
    url?.let { imageUrl ->
        Glide.with(this.context)
            .load(imageUrl).apply {
                if (isCircular)
                    this.circleCrop()

                RequestOptions()
                    .placeholder(android.R.color.darker_gray)
            }.into(this)
    }

}


@BindingAdapter(value = ["price", "discount"], requireAll = false)
fun TextView.priceAfterDiscount(price: String?, discount: Int = 0) {

    var priceAfterDiscount = price
    price?.let {


       val  priceStringValue = price.replace("$","").replace("€","")
        val priceSymbol = price.replace(priceStringValue,"")

        val  priceValue  = priceStringValue.toFloat()


        priceAfterDiscount = "${priceValue.minus(priceValue.times((discount.toFloat()/100)))}$priceSymbol"
    }
    this.text =priceAfterDiscount


}

@BindingAdapter(value = [ "isViewed"])
fun ImageView.isViewed( isViewed: Boolean = false) {
    this.isSelected = isViewed
}

/**
 * submit list to [RecyclerView] if hold adapter of instance [ListAdapter]
 * @param T generic data obejct
 * @param VH viewholder object
 *
 */
@BindingAdapter("submitList")
@Throws
fun <T> RecyclerView.submitList(dataList: List<T>?) {
    adapter.takeIf {
        adapter is ListAdapter<*, *>

    }?.apply {
        dataList?.let { data ->
            (this as ListAdapter<T, *>).submitList(data)
            this.notifyDataSetChanged()

        }
    } ?: throw ClassCastException("can't use submitList as RecyclerView Adapter not ListAdapter")

}
