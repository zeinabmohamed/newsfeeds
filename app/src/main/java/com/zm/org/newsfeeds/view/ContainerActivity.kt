package com.zm.org.newsfeeds.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.navigateUp
import com.zm.org.newsfeeds.R
import com.zm.org.newsfeeds.databinding.ActivityContinerBinding

class ContainerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityContinerBinding>(this, R.layout.activity_continer)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment).navigateUp()
    }

    override fun onBackPressed() {
        if(! findNavController(R.id.nav_host_fragment).popBackStack()){
           super.onBackPressed()
        }

    }


}
