package com.zm.org.newsfeeds.view

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.zm.org.newsfeeds.base.view.BaseFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.zm.org.newsfeeds.R
import com.zm.org.newsfeeds.databinding.FragmentSplashBinding


/**
 * A SplashFragment [Fragment] subclass.
 * the app start point.
 * Use the [SplashFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SplashFragment : BaseFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            inflater,
            R.layout.fragment_splash, container, false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Handler().postDelayed({navigateToHomeFragment()},2000)


    }

    private fun navigateToHomeFragment() {
        findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
    }


    companion object {
        /**
         *
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         */
        @JvmStatic
        fun newInstance() =
            SplashFragment()
    }
}
