package com.zm.org.newsfeeds.view.home


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zm.org.newsfeeds.base.view.BaseFragment
import com.zm.org.newsfeeds.viewmodel.home.HomeFragmentViewModel
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.zm.org.newsfeeds.R
import com.zm.org.newsfeeds.base.view.SpacesItemDecoration
import com.zm.org.newsfeeds.databinding.FragmentHomeBinding
import com.zm.org.newsfeeds.view.news.NewsListAdapter
import com.zm.org.newsfeeds.view.offers.OffersListAdapter
import com.zm.org.newsfeeds.viewmodel.HomeViewModelFactory
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : BaseFragment() {


    private val viewModel: HomeFragmentViewModel by viewModels(factoryProducer = { HomeViewModelFactory() })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var binding: FragmentHomeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        binding.viewmodel = viewModel
        (binding as ViewDataBinding).lifecycleOwner = viewLifecycleOwner
        return (binding as ViewDataBinding).root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.showAllNewsButtonLiveData.observe(this, Observer {

            findNavController().navigate(R.id.action_homeFragment_to_newsListFragment)
        })

        viewModel.showAllOffersButtonLiveData.observe(this, Observer {

            findNavController().navigate(R.id.action_homeFragment_to_offersListFragment)
        })

        storiesRecyclerView.apply {
            adapter = StoriesListAdapter().also {
                addItemDecoration(
                    SpacesItemDecoration(
                        requireContext(),
                        R.dimen.story_item_space
                    )
                )
            }
        }



        newsRecyclerView.apply {
            adapter = NewsListAdapter().also {
                addItemDecoration(
                    SpacesItemDecoration(
                        requireContext(),
                        R.dimen.news_item_space
                    )
                )
            }
        }

        offersRecyclerView.apply {
            adapter = OffersListAdapter().also {
                addItemDecoration(
                    SpacesItemDecoration(
                        requireContext(),
                        R.dimen.offers_item_space
                    )
                )
            }
        }


        viewModel.loadData()


    }

}
