package com.zm.org.newsfeeds.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.zm.org.newsfeeds.databinding.RowStoryItemBinding
import com.zm.org.newsfeeds.model.dto.Story


class StoriesListAdapter :
    ListAdapter<Story, StoriesItemViewHolder>(STORIES_DIFFER_LISTENER_CALLBACK) {
    override fun onBindViewHolder(holder: StoriesItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoriesItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val rowStoryItemBinding = RowStoryItemBinding.inflate(layoutInflater, parent, false)
        return StoriesItemViewHolder(rowStoryItemBinding)
    }


}

class StoriesItemViewHolder(private val rowStoryItemBinding: RowStoryItemBinding) : RecyclerView.ViewHolder(rowStoryItemBinding.root) {

    fun bind(story: Story) {
        rowStoryItemBinding.story = story
        rowStoryItemBinding.run { executePendingBindings() }
    }

}


val STORIES_DIFFER_LISTENER_CALLBACK = object : DiffUtil.ItemCallback<Story>() {
    override fun areContentsTheSame(oldItem: Story, newItem: Story): Boolean = oldItem == newItem


    override fun areItemsTheSame(oldItem: Story, newItem: Story): Boolean = oldItem == newItem
}


