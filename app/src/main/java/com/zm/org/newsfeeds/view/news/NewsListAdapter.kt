package com.zm.org.newsfeeds.view.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.zm.org.newsfeeds.databinding.RowNewsItemBinding
import com.zm.org.newsfeeds.model.dto.News


class NewsListAdapter(private val isSampleList: Boolean = true) :
    ListAdapter<News, NewsItemViewHolder>(NEWS_DIFFER_LISTENER_CALLBACK) {
    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val rowNewsItemBinding = RowNewsItemBinding.inflate(layoutInflater, parent, false)

        if(isSampleList) {
            //setting the height programmatically
            rowNewsItemBinding.root.layoutParams.height = (parent.height / 2) - 10
            rowNewsItemBinding.root.requestLayout()
        }else{
            //setting the height programmatically
            rowNewsItemBinding.root.layoutParams.height = (parent.height / 4) - 10
            rowNewsItemBinding.root.requestLayout()
        }
        return NewsItemViewHolder(rowNewsItemBinding)
    }


}

class NewsItemViewHolder(private val rowNewsItemBinding: RowNewsItemBinding) : RecyclerView.ViewHolder(rowNewsItemBinding.root) {

    fun bind(newsItem: News) {
        rowNewsItemBinding.newsItem = newsItem
        rowNewsItemBinding.run { executePendingBindings() }
    }

}


val NEWS_DIFFER_LISTENER_CALLBACK = object : DiffUtil.ItemCallback<News>() {
    override fun areContentsTheSame(oldItem: News, newItem: News): Boolean = oldItem == newItem


    override fun areItemsTheSame(oldItem: News, newItem: News): Boolean = oldItem == newItem
}


