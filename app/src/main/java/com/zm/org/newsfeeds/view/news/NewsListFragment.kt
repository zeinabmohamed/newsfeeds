package com.zm.org.newsfeeds.view.news


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zm.org.newsfeeds.base.view.BaseFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import com.zm.org.newsfeeds.R
import com.zm.org.newsfeeds.base.view.SpacesItemDecoration
import com.zm.org.newsfeeds.databinding.FragmentNewsBinding
import com.zm.org.newsfeeds.viewmodel.NewsViewModelFactory
import com.zm.org.newsfeeds.viewmodel.news.NewsFragmentViewModel
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * A simple [Fragment] subclass.
 * Use the [NewsListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NewsListFragment : BaseFragment() {


    private val viewModel: NewsFragmentViewModel by viewModels(factoryProducer = { NewsViewModelFactory() })


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var binding: FragmentNewsBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_news, container, false)

        binding.viewmodel = viewModel
        (binding as ViewDataBinding).lifecycleOwner = viewLifecycleOwner
        return (binding as ViewDataBinding).root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        newsRecyclerView.apply {
            adapter = NewsListAdapter(false).also {
                addItemDecoration(
                    SpacesItemDecoration(
                        requireContext(),
                        R.dimen.news_item_space
                    )
                )
            }
        }

        viewModel.loadNews()

    }


}
