package com.zm.org.newsfeeds.view.offers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.zm.org.newsfeeds.databinding.RowOffersItemBinding
import com.zm.org.newsfeeds.model.dto.Offer


class OffersListAdapter(private val isSampleList: Boolean = true) :
    ListAdapter<Offer, OfferItemViewHolder>(OFFERS_DIFFER_LISTENER_CALLBACK) {
    override fun onBindViewHolder(holder: OfferItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val rowNewsItemBinding = RowOffersItemBinding.inflate(layoutInflater, parent, false)
        if(!isSampleList){
            //setting the height programmatically
            rowNewsItemBinding.root.layoutParams.height = (parent.height / 4) - 10
            rowNewsItemBinding.root.requestLayout()
        }


        return OfferItemViewHolder(rowNewsItemBinding)
    }


}

class OfferItemViewHolder(private val rowOffersItemBinding: RowOffersItemBinding) : RecyclerView.ViewHolder(rowOffersItemBinding.root) {

    fun bind(offer: Offer) {
        rowOffersItemBinding.offer = offer
        rowOffersItemBinding.run { executePendingBindings() }
    }

}


val OFFERS_DIFFER_LISTENER_CALLBACK = object : DiffUtil.ItemCallback<Offer>() {
    override fun areContentsTheSame(oldItem: Offer, newItem: Offer): Boolean = oldItem == newItem


    override fun areItemsTheSame(oldItem: Offer, newItem: Offer): Boolean = oldItem == newItem
}


