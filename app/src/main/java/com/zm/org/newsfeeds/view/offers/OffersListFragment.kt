package com.zm.org.newsfeeds.view.offers


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zm.org.newsfeeds.base.view.BaseFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import com.zm.org.newsfeeds.R
import com.zm.org.newsfeeds.base.view.SpacesItemDecoration
import com.zm.org.newsfeeds.databinding.FragmentOffersBinding
import com.zm.org.newsfeeds.viewmodel.OffersViewModelFactory
import com.zm.org.newsfeeds.viewmodel.offers.OffersFragmentViewModel
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * A simple [Fragment] subclass.
 * Use the [OffersListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OffersListFragment : BaseFragment() {


    private val viewModel: OffersFragmentViewModel by viewModels(factoryProducer = { OffersViewModelFactory() })


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var binding: FragmentOffersBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_offers, container, false)

        binding.viewmodel = viewModel
        (binding as ViewDataBinding).lifecycleOwner = viewLifecycleOwner
        return (binding as ViewDataBinding).root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        offersRecyclerView.apply {
            adapter = OffersListAdapter(false).also {
                addItemDecoration(
                    SpacesItemDecoration(
                        requireContext(),
                        R.dimen.news_item_space
                    )
                )
            }
        }

        viewModel.loadOffers()

    }


}
