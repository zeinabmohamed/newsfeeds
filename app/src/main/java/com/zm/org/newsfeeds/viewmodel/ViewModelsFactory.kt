package com.zm.org.newsfeeds.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zm.org.newsfeeds.model.NewsFeedsDataModel
import com.zm.org.newsfeeds.viewmodel.home.HomeFragmentViewModel
import com.zm.org.newsfeeds.viewmodel.news.NewsFragmentViewModel
import com.zm.org.newsfeeds.viewmodel.offers.OffersFragmentViewModel

/**
 * ViewModel Factory used as direct injection in demo size
 * but in large scale projects can be done using injection 3'rd party
 */

class HomeViewModelFactory : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeFragmentViewModel(NewsFeedsDataModel()) as T

    }
}


class NewsViewModelFactory : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsFragmentViewModel(NewsFeedsDataModel()) as T

    }
}

class OffersViewModelFactory : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OffersFragmentViewModel(NewsFeedsDataModel()) as T

    }
}