package com.zm.org.newsfeeds.viewmodel.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zm.org.newsfeeds.base.viewmodel.BaseViewModel
import com.zm.org.newsfeeds.model.NewsFeedsDataModel
import com.zm.org.newsfeeds.model.dto.News
import com.zm.org.newsfeeds.model.dto.Offer
import com.zm.org.newsfeeds.model.dto.Story
import kotlinx.coroutines.launch

class HomeFragmentViewModel(private  val newsFeedsDataModel: NewsFeedsDataModel)  : BaseViewModel() {

    val storiesListLiveData: MutableLiveData<List<Story>>  = MutableLiveData()
    val newsListLiveData: MutableLiveData<List<News>>  = MutableLiveData()
    val offersListLiveData: MutableLiveData<List<Offer>>  = MutableLiveData()
    val showAllNewsButtonLiveData: MutableLiveData<Unit>  = MutableLiveData()
    val showAllOffersButtonLiveData: MutableLiveData<Unit>  = MutableLiveData()


     fun loadData() {

       // TODO("show loading")
        // load stories
         try {
       val storiesResult =       viewModelScope.launch {
                 newsFeedsDataModel.loadStories().also {
                     storiesListLiveData.value = it.data
                 }
             }

          var newsResult =  viewModelScope.launch {
                 newsFeedsDataModel.loadNews().also {
                     newsListLiveData.postValue(it.data.slice(IntRange(0,3)))
                 }
             }


             var offersResult =  viewModelScope.launch {
                 newsFeedsDataModel.loadOffers().also {
                     offersListLiveData.postValue(it.data.slice(IntRange(0,3)))
                 }
             }

         }catch (ex :Exception){
             ex.printStackTrace()
         }

        // TODO("hide loading")

    }

    fun showAllNewsButtonClicked(){
        showAllNewsButtonLiveData.postValue(Unit)
    }

    fun showAllOffersButtonClicked(){
        showAllOffersButtonLiveData.postValue(Unit)
    }
}
