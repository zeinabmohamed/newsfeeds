package com.zm.org.newsfeeds.viewmodel.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zm.org.newsfeeds.base.viewmodel.BaseViewModel
import com.zm.org.newsfeeds.model.NewsFeedsDataModel
import com.zm.org.newsfeeds.model.dto.News
import com.zm.org.newsfeeds.model.dto.Offer
import com.zm.org.newsfeeds.model.dto.Story
import kotlinx.coroutines.launch

class NewsFragmentViewModel(private  val newsFeedsDataModel: NewsFeedsDataModel)  : BaseViewModel() {

    val newsListLiveData: MutableLiveData<List<News>>  = MutableLiveData()


     fun loadNews() {

       // TODO("show loading")
        // load stories
         try {

          var newsResult =  viewModelScope.launch {
                 newsFeedsDataModel.loadNews().also {
                     newsListLiveData.postValue(it.data)
                 }
             }

         }catch (ex :Exception){
             ex.printStackTrace()
         }

        // TODO("hide loading")


    }
}
