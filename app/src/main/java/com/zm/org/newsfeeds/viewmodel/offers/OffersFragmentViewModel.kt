package com.zm.org.newsfeeds.viewmodel.offers

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zm.org.newsfeeds.base.viewmodel.BaseViewModel
import com.zm.org.newsfeeds.model.NewsFeedsDataModel
import com.zm.org.newsfeeds.model.dto.Offer
import kotlinx.coroutines.launch

class OffersFragmentViewModel(private  val newsFeedsDataModel: NewsFeedsDataModel)  : BaseViewModel() {

    val offersListLiveData: MutableLiveData<List<Offer>>  = MutableLiveData()


     fun loadOffers() {

       // TODO("show loading")
        // load stories
         try {

          var newsResult =  viewModelScope.launch {
                 newsFeedsDataModel.loadOffers().also {
                     offersListLiveData.postValue(it.data)
                 }
             }

         }catch (ex :Exception){
             ex.printStackTrace()
         }

        // TODO("hide loading")


    }
}
