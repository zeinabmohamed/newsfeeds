object Versions {
    object Android {
        const val buildToolsVersion = "29.0.2"
        const val compileSdkVersion = 29
        const val minSdkVersion = 21
        const val targetSdkVersion = 29

        const val appVersionCode = 1
        const val appVersionName = "1.0"
        const val applicationId = "com.zm.org.newsfeeds"


    }

    const val glide_version: String ="4.9.0"
    const val retrofit_version: String = "2.6.1"
    const val life_cycle_owner_version = "2.2.0-alpha03"

    const val material_desgin_version: String = "1.1.0-alpha10"
    const val nav_version: String = "2.1.0"
    const val constraintLayoutVersion: String = "1.1.3"
    const val appcompat: String = "1.1.0"

    object Gradle {
        const val androidGradlePluginVersion = "3.5.0"
    }

    object Test {
        const val junit_version: String = "4.12"
        const val espresso_core_version: String = "3.2.0"
        const val testExtJunit: String = "1.1.1"
    }


    const val kotlinVersion = "1.3.50"

}
